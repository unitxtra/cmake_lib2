//
// Created by Anders Cedronius on 2020-04-21.
//

#ifndef CMAKE_LIB2_SOMEOTHERCLASS_H
#define CMAKE_LIB2_SOMEOTHERCLASS_H


class SomeOtherClass {
public:
    int addThreeNumbers(int,int,int);
};


#endif //CMAKE_LIB2_SOMEOTHERCLASS_H
