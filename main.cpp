#include <iostream>
#include "SomeOtherClass.h"


int main() {
    std::cout << "Add three numbers" << std::endl;

    SomeOtherClass mySomeOtherClass;

    int g = 5 + 7 + 12;
    int h = mySomeOtherClass.addThreeNumbers(5,7,12);

    if (g != h) {
        std::cout << "test fail" << std::endl;
    } else {
        std::cout << "test ok" << std::endl;
    }

    return 0;
}
